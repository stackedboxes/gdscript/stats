# Statistics

*Part of [StackedBoxes' GDScript Hodgepodge](https://gitlab.com/stackedboxes/gdscript/)*

Simple statistical functions to compute things like mean, variance and standard
deviation of arrays of numbers.
