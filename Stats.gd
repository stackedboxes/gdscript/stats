#
# Statistics
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c)2019 Leandro Motta Barros
#

extends Node


# Returns the sum of the values in the passed data array.
static func sum(data: Array) -> float:
	var sum := 0.0

	for d in data:
		sum += d

	return sum


# Returns the mean of an array of numbers. If the array is empty, returns NAN.
static func mean(data: Array) -> float:
	var n := data.size()
	if n == 0:
		return NAN

	var s := sum(data)

	return s / n


# Returns the (population) variance of an array of numbers. If you happen to
# know the mean of the data in the array, you can pass it as the optional second
# parameter to make the calculation a quicker. If the array is empty, returns
# NAN.
static func variance(data: Array, m: float = NAN) -> float:
	var acc := 0.0
	var n := data.size()

	if n == 0:
		return NAN

	if is_nan(m):
		m = mean(data)

	for d in data:
		var v := float(d) - m
		acc += v * v

	return acc / n


# Returns the (population) standard deviation of an array of numbers. If you
# have the mean of the data on this array, you can pass it as the optional
# second parameter -- this will make the execution faster. If the array is
# empty, returns NAN.
static func standardDeviation(data: Array, m: float = NAN) -> float:
	return sqrt(variance(data, m))
