#
# Statistics
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2019 Leandro Motta Barros
#

extends SceneTree

var Stats
var Assert

func _init():
	Stats = preload("res://Stats.gd")
	Assert = preload("res://Assert.gd")

	Assert.isEqual(7, Stats.sum([1, 2, 4]))
	Assert.isEqual(9.0, Stats.sum([3.0, -1.0, 0.0, 2.0, 2.0, 1.0, -5.0, 7.0]))
	Assert.isEqual(143.0, Stats.sum([1.0, 1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 34.0, 55.0]))
	Assert.isEqual(111.0, Stats.sum([111.0]))
	Assert.isEqual(0.0, Stats.sum([]))

	Assert.isClose(2.3333333333, Stats.mean([1, 2, 4]), 1e-8)
	Assert.isEqual(1.125, Stats.mean([3.0, -1.0, 0.0, 2.0, 2.0, 1.0, -5.0, 7.0]))
	Assert.isEqual(14.3, Stats.mean([1.0, 1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 34.0, 55.0]))
	Assert.isNaN(Stats.mean([]))

	Assert.isClose(1.555555555555555, Stats.variance([1, 2, 4]), 1e-8)
	Assert.isClose(1.555555555555555, Stats.variance([1, 2, 4], 2.3333333333), 1e-8)
	Assert.isClose(10.359375, Stats.variance([3.0, -1.0, 0.0, 2.0, 2.0, 1.0, -5.0, 7.0]), 1e-8)
	Assert.isClose(10.359375, Stats.variance([3.0, -1.0, 0.0, 2.0, 2.0, 1.0, -5.0, 7.0], 1.125), 1e-8)
	Assert.isClose(285.01, Stats.variance([1.0, 1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 34.0, 55.0]), 1e-8)
	Assert.isClose(285.01, Stats.variance([1.0, 1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 34.0, 55.0], 14.3), 1e-8)
	Assert.isNaN(Stats.variance([]))

	Assert.isClose(1.24721912892464, Stats.standardDeviation([1, 2, 4]), 1e-8)
	Assert.isClose(1.24721912892464, Stats.standardDeviation([1, 2, 4], 2.3333333333), 1e-8)
	Assert.isClose(3.21859829, Stats.standardDeviation([3.0, -1.0, 0.0, 2.0, 2.0, 1.0, -5.0, 7.0]), 1e-8)
	Assert.isClose(3.21859829, Stats.standardDeviation([3.0, -1.0, 0.0, 2.0, 2.0, 1.0, -5.0, 7.0], 1.125), 1e-8)
	Assert.isClose(16.8822391, Stats.standardDeviation([1.0, 1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 34.0, 55.0]), 1e-8)
	Assert.isClose(16.8822391, Stats.standardDeviation([1.0, 1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 34.0, 55.0], 14.3), 1e-8)
	Assert.isNaN(Stats.variance([]))

	quit()
